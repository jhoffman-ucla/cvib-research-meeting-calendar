<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title id="site_title">Login</title>
    <link rel="stylesheet" href="/static/minimal.css">
  </head>
  <body>

	<form action="{{ .URL }}" method="post">
	  <label for="username">Username</label>
	  <input type="text" id="username" placeholder="username" name="username" required>

	  <label for="password">Password</label>
	  <input type="password" id="password" placeholder="password" name="password" required>

	  <button type="submit">Login</button>
	</form>
	
  </body>
</html>