# CVIB Research Meeting Scheduling Instructions

First of all, thank you *so much*! This is the stuff I really struggle with. :) 

---

## Key tasks
1. [Get speakers scheduled](#scheduling-a-speaker) via this website ([cvibresearchmeeting.info/](/))
1. Get [Title/Abstract from author](#following-up) preferably ~1 week in advance
1. Prepare research meeting [Wednesday email](#wednesdays)
1. Ensure that the recurring invite exists in Outlook (can be owned by my calendar? not sure how these big group events work)
1. Make sure the 615 conference room remains booked for the meeting (or alternative)

---

## John's involvement

**Small (but important) stuff:**
- I'm always available if there are questions and concerns!
- If there's any feedback to make this site better or improve the process, let me know!

If there's something urgent, please reach out via text or phone call :) 

**Bigger stuff:**
- I'd like to remain the one doing the initial interfacing with potential speakers to vet what goes on the calendar.
  - If someone reaches out directly to you without me, please redirect them to me as the first point of contact.
- I'd like to remain the one to actually send the email out since I'd like to maintain the continuity and do a final review before it goes out.

---
<a name="wednesdays"></a>
### Wednesday Email

This is really the bulk of the week-to-week work (why I'm putting it at the top!).  Other than this, it's pretty ad-hoc scheduling and stuff. Nothing too complex!

Ideally a notice goes out on Wednesday in the late afternoon for the meeting on the next day.

To generate this email, just click the "announcement email" link at the top of the page.

Or just click this: [https://cvibresearchmeeting.info/announcementemail](/announcementemail)

Copy and paste the contents of the page into an email (unfortunately the "copy to clipboard" button does *not* work at the moment!)

Please verify the following:
- Subject line is removed from the body of the email and put into the subject of the email
- Current week speakers
- Upcoming speakers (or remove section if no upcoming speakers)
- Overall grammatical correctness 

Please send the email to me ([jmhoffman@mednet.ucla.edu](mailto:jmhoffman@mednet.ucla.edu)) and I will add some comments and send on to the big group!

Whenever this email goes out would also be a good time to reach out and [follow up](#following-up) with the authors for the next week.

---

## How to use this site

### Logging in

If you're seeing this page, you're logged in!

Login via the (slightly hidden) [login page](/login)

*The rest of these instructions assume that you're logged in*

John will provide the username and password to connect to this site.

There's no link to log in on the main page for security reasons, so you'll likely want to bookmark the [login page](/login).  You'll be logged out after 24 hours of not using the site.

<a name="scheduling-a-speaker"></a>
### Scheduling a Speaker 

#### Scheduling Email

The website can auto-generate an email with the next 5 weeks of availability for the research meeting, as well as some basic instructions for folks who want to present.

To access, click the ["scheduling email" link](/schedulingemail) at the top of the page.  You can copy and paste that text (or any subsection of it) into an email.

Please also feel free to wording as needed/desired.  I rarely use the exact template.

#### Putting them on the calendar

Once the speaker(s) pick a time, click the **"reserve"** link next to their chosen day.  Fill out the form, adjusting for the requested time.

Please ensure at least the following are populated:
- Date, start time, stop time
- Speaker
- Affiliation (department or university)
- Talk Title

Please have the authors provide a title (even if it's temporary) at the time of putting them on the calendar.  I haven't been good at enforcing this, so there's a chance you may get some pushback, but I'd like to slowly work to change it.

<a name="following-up"></a>
#### Following up with authors

About a week in advance of the meeting, we should follow up with the authors and 

1. Verify they're still able to present (and remember that they're presenting!)
1. Ensure that we have an up-to-date title and description for their talk
1. Provide them with the zoom info in advance if requested (CVIB folks usually already have it, but folks presenting from other institutions do not).

#### Additional scheduling guidelines

- **Aim for only one speaker per meeting.**  It's ok to have two, but there should be a good reason (upcoming conference deadline, out-of-town speaker with only availability on day/time, etc.)
- **We always start the meeting at 3,** so the 4-5pm is generally *only* available if there's someone speaking 3-4pm. We can occasionally make an exception for this, but is should also be a "good reason" kind of thing. 
- I try to be **careful to not schedule sessions during big conferences** where lots of folks will be out of town.  I don't know if you have those dates already, or if not what the best way for us to communicate them to you is!  Let's discuss.
- If someone wants to schedule beyond 5 weeks out, please let me know.  I avoid this generally, but it does come up from time to time.  There's a "secret" way to do it via the website. haha.

- Prefer flexibility, but don't make your life miserable!

I try to do my best to accomodate last minute schedule changes, missing titles, etc. since this is really supposed to be a pretty relaxed meeting to exchange latest ideas and prep current work for presentation.  It's also mainly our students and colleagues presenting.  As the person helping with the schedule though, you can put your foot down on the academic flakiness (including me)!  I've got your back and support it! :) 

