<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title id="site_title">Edit Talk</title>
		<link rel="stylesheet" href="/static/minimal.css">
	</head>
	<body>

		<h1> Edit talk </h1>
		<hr class="dotted">

		<form action="/talk/{{ .Id }}" method="post">
			<ul>

                <li><input type="hidden" id="id" name="id" value="{{ .Id }}" ></li>

				<li>
					<!-- YYYY-MM-DD -->
					<label for="date">Date:</label>
					<input type="date" id="date" name="date" value={{ .Interval.Start | formdate}}>
				</li>

				<li>
					<label for="starttime">Start Time:</label>
					<input type="time" id="starttime" name="starttime" value="{{ .Interval.Start | formtimecolons }}" >
				</li>

				<li>
					<label for="endtime">End Time:</label>
					<input type="time" id="endtime" name="endtime" value="{{ .Interval.End | formtimecolons }}" >
				</li>

				<!-- <li>
					 <label for="starttime">Start Time:</label>
					 <input type="text" id="starttime" name="starttime" placeholder="YYYY/MM/DD HH:MM" value="{{ .Interval.Start | edittime }}">
				     </li>

				     <li>
					 <label for="endtime">End Time:</label>
					 <input type="text" id="endtime" name="endtime" placeholder="YYYY/MM/DD HH:MM" value="{{ .Interval.End | edittime }}">
				     </li> -->

				<li>
					<label for="speaker">Speaker:</label>
					<input type="text" id="speaker" name="speaker" value="{{ .Speaker }}">
				</li>

				<li>
					<label for="affiliation">Affiliation:</label>
					<input type="text" id="affiliation" name="affiliation" value="{{ .Affiliation }}">
				</li>

				<li>
				    <label for="private">Private:</label>
				    <input type="checkbox" id="private" name="private" {{ if .Private }} checked {{ end }}>
				</li>

				<li>
					<label for="title">Talk title:</label>
					<input type="text" id="title" name="title" value="{{ .Title }}">
				</li>

				<li>
					<label for="description">Talk description:</label>
					<input type="text" id="description" name="description" value="{{ .Description }}">
				</li>

				<li>
					<label for="recordinglink">Recording Link:</label>
					<input type="text" id="recordinglink" name="recordinglink" value="{{ .RecordingLink }}">
				</li>

				<li class="button">
					<button type="submit">Save</button> 
				</li>

			</ul>
		</form>
		
	</body>
</html>
