<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<title id="site_title">Announcement Email Generator</title>
	<link rel="stylesheet" href="static/minimal.css">
  </head>
  <body>

	<center><a href="/">Home</a></center>

	<pre id="generatedemail" class="copyable">

SUBJECT: CVIB Research Meeting - {{ .DateAndTime }}
	  
Hello All,

<!-- If we have talks for the week: -->
{{ if gt .NumTalks 0 }}
This week we have {{ .NumTalksStr }} exciting talks on the schedule:
{{ range .Talks  }}
{{ .Start}}-{{.End}} - <b>{{.Speaker}}</b> - <em>{{ .Title }}</em>{{ end }}
{{ range .Talks  }}
<b>Speaker:</b> {{ .Speaker }}
<b>Affiliation:</b> {{ .Affiliation }}
<b>Topic:</b> {{ .Title }}
<b>Description:</b> {{ .Description }}
{{ end }}
<b>Zoom Meeting Info:</b>
https://uclahs.zoom.us/j/99808712846?pwd=WldVNTFJMjl3bkVLRFdhZzRRd2pLQT09
Password: 444162

One tap mobile

+16699006833,,99808712846# US (San Jose)
+12532158782,,99808712846# US (Tacoma)

Dial by your location
        +1 669 900 6833 US (San Jose)
        +1 253 215 8782 US (Tacoma)
        +1 346 248 7799 US (Houston)
        +1 312 626 6799 US (Chicago)
        +1 646 876 9923 US (New York)
        +1 301 715 8592 US (Washington DC)

Meeting ID: 998 0871 2846
Password: 444162
Find your local number: https://uclahs.zoom.us/u/abxYynHa8W
{{ end }}

<b>Upcoming Talks</b>{{ range .Upcoming }}{{if .Private}}{{continue}}{{end}}
- {{ .Date }} - {{ .Start}}-{{ .End }} - {{ .Speaker }}, {{ .Affiliation }} - <em> {{ .Title }}</em>{{ end }}

<b>Recordings of Previous CVIB Research Meetings:</b>{{ range .Recordings}}{{if .Private}}{{continue}}{{end}}
- <b> {{ .Speaker }} - {{ .Title }} </b>
    {{ .RecordingLink }}{{ end }}

Recordings for all previous talks can be found at <a href="https://cvibresearchmeeting.info">cvibresearchmeeting.info</a>.

As always, reach out if you have any questions or any problems connecting.

Thanks and see you there!

Best,
John
	</pre>

	<button onclick="copyfunc()"> Copy to clipboard </button>

	<script>

	  /* Get the text field */
	  let obj = document.getElementById("generatedemail");
	  
	  /* Copy the text inside the text field */
	  navigator.clipboard.writeText(obj.innerHTML);

	  /* Alert the copied text */
	  alert("Copied the text: " + copyText.value);

	</script>
  </body>
</html>


<!-- <table> -->
<!--   <tr> -->
<!-- 	<th> <b>Time</b> </th> -->
<!-- 	<th> <b>Speaker</b> </th> -->
<!-- 	<th> <b>Topic</b> </th> -->
<!--   </tr> -->

<!--   {{ range .Talks }} -->
<!--   <tr> -->
<!--   <td> {{ .Start}}-{{.End}} </td> -->
<!--   <td> <b>{{.Speaker}} </b> </td> -->
<!--   <td> <em>{{ .Title }}</em> </td> -->
<!--   </tr> -->
<!--   {{ end }} -->

<!-- </table> -->


