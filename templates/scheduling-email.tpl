<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<title id="site_title">Scheduling Email Generator</title>
	<link rel="stylesheet" href="static/minimal.css">
  </head>
  <body>

	<center><a href="/">Home</a></center>

	<pre id="generatedemail">
We have the following upcoming time slots available:
{{ range . }}
- {{.Date}} - {{.Start}}-{{.End}}{{ end }}

Please send me an email letting me know the following:
- Which slot you'd prefer and how long you'd like me to schedule you for,
- A title for your talk, and
- A 1-3 sentence description that I can including when I send out the weekly announcement

Generally speaking, an hour long is pretty safe estimate for most presentations and discussion, however we can schedule for less or more as you see fit.

Finally, just a heads up that regardless of your slot start time, the research meeting typically begins at 3pm.  If a time slot lists a start time later than than 3pm, it should generally be understood to mean "<that time> or whenever the prior presentation concludes."  On the day you're presenting, please plan to attend the meeting starting at 3pm.

Thanks and looking forward to hearing from you!

Best,
John
	</pre>

	<button onclick="copyfunc()"> Copy to clipboard </button>

	<script>

	  /* Get the text field */
	  let obj = document.getElementById("generatedemail");
	  
	  /* Copy the text inside the text field */
	  navigator.clipboard.writeText(obj.innerHTML);

	  /* Alert the copied text */
	  alert("Copied the text: " + copyText.value);

	</script>
  </body>
</html>