<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title id="site_title">CVIB-RM Home</title>
		<link rel="stylesheet" href="static/minimal.css">
	</head>
	<body>

		<h1> Add new talk </h1>
		<hr class="dotted">

		<form action="/talk/new" method="post">
			<ul>

				<li>
					<!-- YYYY-MM-DD -->
					<label for="date">Date:</label>
					{{ if .Date }}
					<input type="date" id="date" name="date" value={{ .Date }}>
					{{ else }}
					<input type="date" id="date" name="date">
					{{ end }}
				</li>

				<li>
					<label for="starttime">Start Time:</label>
					<!-- <input type="text" id="starttime" name="starttime" placeholder="YYYY/MM/DD HH:MM"> -->
					{{ if .Start }}
					<input type="time" id="starttime" name="starttime" value="{{ .Start }}" >
					{{ else }}
					<input type="time" id="starttime" name="starttime" value="15:00">
					{{ end }}
				</li>

				<li>
					<label for="endtime">End Time:</label>
					<!-- <input type="text" id="endtime" name="endtime" placeholder="YYYY/MM/DD HH:MM"> -->
					{{ if .End }}
					<input type="time" id="endtime" name="endtime" value="{{ .End }}" >
					{{ else }}
					<input type="time" id="endtime" name="endtime" value="17:00">
					{{ end }}
				</li>

				<li>
					<label for="speaker">Speaker:</label>
					<input type="text" id="speaker" name="speaker">
				</li>

				<li>
					<label for="affiliation">Affiliation:</label>
					<input type="text" id="affiliation" name="affiliation">
				</li>

				<li>
				    <label for="private">Private:</label>
				    <input type="checkbox" id="private" name="private">
				</li>
				
				<li>
					<label for="title">Talk title:</label>
					<input type="text" id="title" name="title">
				</li>

				<li>
					<label for="description">Talk description:</label>
					<input type="text" id="description" name="description">
				</li>

				<li class="button">
					<button type="submit">Submit</button>
				</li>

			</ul>
		</form>
		
	</body>
</html>
