<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<title id="site_title">Announcement Email Generator</title>
	<link rel="stylesheet" href="static/minimal.css">
  </head>
  <body>

	  <center><a href="/">Home</a></center>

	  <pre id="generatedemail" class="copyable">

SUBJECT: CVIB Research Meeting - NO MEETING - {{ .DateAndTime }}

Hello All,

Nothing on the calendar for this week!  Please reach out if you would like to get on the calendar for one of the upcoming slots.

Upcoming talks, available slots, and recordings can be found at <a href="https://cvibresearchmeeting.info">cvibresearchmeeting.info</a>.

Thanks!

Best,
John
	  </pre>

  </body>
  
</html>