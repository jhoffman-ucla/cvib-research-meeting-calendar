<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title id="site_title">CVIB-RM Home</title>
    <link rel="stylesheet" href="/static/minimal.css">
  </head>
  <body>
      <center><h1 id="header">CVIB Research Meeting Calendar</h1></center>

	  <!-- <hr class="dotted"> -->
	  {{ if .Authenticated }}
	  <center> <a href="schedulingemail">scheduling email</a> | <a href="announcementemail">announcement email </a> | <a href="nomeetingemail">no meeting email</a> | <a href="md/instructions">instructions</a></center>
	  {{ end }}

	  <h2> Upcoming </h2>
	  <hr class="dotted">
	  <table>
		  {{ range .Upcoming }}
		  <tr>
			  <td>{{ .Interval.Start | shortdate }}</td>
			  <td>{{ .Interval.Start | shorttime }}-{{ .Interval.End | shorttime }}</td>
			  <td>{{ .Speaker }}</td>
			  <td><em> {{ if .Private}}(private){{ end}} {{ .Title }}</em></td>
			  {{ if $.Authenticated }}
			  <td><a href="/edit/{{ .Id }}"> edit </a> </td>
			  <td><a href="#" onclick="confirmdelete('{{ .Id }}')"> delete </a> </td>
			  {{ end }}
		  </tr>
		  {{ end }}
	  </table>
	  
	  <h2> Upcoming - Free Slots </h2>
	  <hr class="dotted">
	  <table>
		  {{ range .UpcomingFree }}
		  <tr>
			  <td>{{ .Start | shortdate }} </td>
			  <td>{{ .Start | shorttime }}-{{ .End | shorttime }} </td>
			  {{ if $.Authenticated }}
			  <td><a href="/addnew?date={{ .Start | formdate }}&start={{ .Start | formtime }}&end={{ .End | formtime}}">reserve</a></td>
			  {{ end }}
		  </tr>
		  {{ end }}
	  </table>
	  
	  <h2> Past Talks </h2>
	  <hr class="dotted">
	  <table>
		  {{ range .Past }}

		  {{ if .Private }} {{continue}} {{end}}
		
		  <tr>
			  <td>{{ .Interval.Start | shortdate }}</td>
			  <td>{{ .Interval.Start | shorttime }}-{{ .Interval.End | shorttime }}</td>
			  <td>{{ .Speaker }}</td>
			  <td><em>{{ .Title }}</em></td>
			         
			  {{ if $.Authenticated }}
			  <td><a href="/edit/{{ .Id }}"> edit </a> </td>
			  {{ end }}
			  
			  {{ if .RecordingLink }}
			  <td><a href="{{ .RecordingLink }}"> recording </a> </td>
			  <!-- <td style="text-align:center;">{{ .RecordingLink | passwd }}  </td> -->
			  <td style="text-align:center;"><a href="javascript:;" onclick="retrievepasscode(this, '{{ .Id }}')">passcode</a></td>
			  {{ else }}
			  <td> recording </td>
			  <td style="text-align:center;"> n/a </td>
			  {{ end }}
			  
		  </tr>
		  {{ end }}
	  </table>
	  
	  <script src="static/script.js?version=1.0.0"></script>
  </body>


  
  <footer>
	<hr>
	<center><p> This site is developed and maintained by <a href="https://johnmarianhoffman.com">John Hoffman</a>.  Please reach out with any comments or questions.</center>
  </footer>
</html>
