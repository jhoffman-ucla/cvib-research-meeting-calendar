FROM --platform=linux/amd64 golang:alpine AS builder

RUN mkdir /app
COPY ./ /app/
WORKDIR /app/
RUN go mod download
RUN go build .

FROM alpine:latest as runner
ENV TZ=America/Los_Angeles
RUN apk add --no-cache tzdata
COPY --from=builder /app/ /app
WORKDIR /app
ENTRYPOINT ["./cvib-research-meeting-calendar"]
