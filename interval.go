package main

import (
	//"fmt"
	"time"
)

type Interval struct {
	Start time.Time `json:"start"`
	End   time.Time `json:"end"`
}

func (in Interval) Contains(t time.Time) bool {
	//return (t.After(in.Start) || t.Equal(in.Start)) && ( t.Before(in.End) || t.Equal(in.End))
	return (t.After(in.Start)) && (t.Before(in.End))
}

func (in Interval) String() string {
	date := in.Start.Format("2006/01/02")
	start := in.Start.Format("3:04")
	end := in.End.Format("3:04")
	return date  + " - " + start + "-" + end
}

func removeInterval(in_to_remove Interval, intervs []Interval) []Interval {
	
	final := make([]Interval, 0)

	for {
		if len(intervs) == 0 {
			break
		}
		// Grab first interval from slice
		curr_interv := intervs[0]
		intervs = intervs[1:]
		
		final = append(final, curr_interv.GetRemainder(in_to_remove)...)
	}
	
	return final
}

func (in Interval) GetRemainder(other_in Interval) []Interval {

	// Other interval fully contains the current interval
	if other_in.Contains(in.Start) && other_in.Contains(in.End) {
		return nil
	}

	// Intervals are equal
	if other_in.Start.Equal(in.Start) && other_in.End.Equal(in.End) {
		return nil
	}
	
	// Other_in is NOT contained at all 
	if !in.Contains(other_in.Start) && !in.Contains(other_in.End) {
		return []Interval{in}
	}

	// Contains beginning but not end
	if in.Contains(other_in.Start) && !in.Contains(other_in.End) {
		new_interval := Interval{
			Start: in.Start,
			End:   other_in.Start,
		}
		return []Interval{new_interval}
	}

	// Contains end but not beginning
	if !in.Contains(other_in.Start) && in.Contains(other_in.End) {
		new_interval := Interval{
			Start: other_in.End,
			End:   in.End,
		}

		return []Interval{new_interval}
	}

	// Fully contained
	if in.Contains(other_in.Start) && in.Contains(other_in.End) {
		new_interval_1 := Interval{
			Start: in.Start,
			End:   other_in.Start,
		}

		new_interval_2 := Interval{
			Start: other_in.End,
			End:   in.End,
		}

		return []Interval{new_interval_1, new_interval_2}
	}

	return nil
}
