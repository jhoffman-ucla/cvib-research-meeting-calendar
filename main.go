package main

import (
	"net/http"
	"os"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"fmt"
)

var ds DataStore

var db_save_path = "data/db.json"

func init() {
	log.SetFormatter(&log.TextFormatter{
		ForceColors:     true,
		DisableColors:   false,
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02 15:04:05",
	})

	//customFormatter := new(logrus.TextFormatter)
	//customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	//logrus.SetFormatter(customFormatter)

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)
}

func main() {

	fmt.Println("---------------- ❤️❤️❤️ ----------------")

	ds = NewMemoryStore()
	err := ds.Load()
	if err != nil {
		log.Fatal("Attempted to load db.json but encountered error: ", err)
	}

	// Log all routes
	r := mux.NewRouter().StrictSlash(true)
	r.Use(LoggingMiddleware)
	r.Use(VerifyCookiesMiddleware)

	// Should *not* use the authentication middleware
	r_open := r.PathPrefix("/").Subrouter().StrictSlash(true)
	r_open.HandleFunc("/login", LoginHandler)
	r_open.HandleFunc("/refresh", RefreshHandler)
	r_open.HandleFunc("/talks", TalksHandler)
	r_open.HandleFunc("/pass/{id}", PasscodeHandler)

	// Should set "authenticated" but not deny access to resource
	r_auth := r.PathPrefix("/").Subrouter().StrictSlash(true)
	r_auth.Use(AuthMiddleware)
	r_auth.HandleFunc("/", IndexHandler)

	// Deny access to endpoint if not authenticated
	r_protected := r_auth.PathPrefix("/").Subrouter().StrictSlash(true)
	r_protected.Use(ProtectedMiddleware)
	r_protected.HandleFunc("/talk/{id}", TalkHandler)
	r_protected.HandleFunc("/addnew", AddNewFormHandler)
	r_protected.HandleFunc("/edit/{id}", EditHandler)
	r_protected.HandleFunc("/schedulingemail", SchedulingHandler)
	r_protected.HandleFunc("/announcementemail", AnnouncementHandler)
	r_protected.HandleFunc("/nomeetingemail", NoMeetingHandler)
	r_protected.HandleFunc("/md/{page}", MarkdownHandler)

	// Serve static files from static dir
	fs := http.FileServer(http.Dir("./static"))
	r.PathPrefix("/").Handler(http.StripPrefix("/static", fs))

	http.Handle("/", r)
	//log.Fatal(http.ListenAndServe("cvibresearchmeeting.info:8080", r))
	addr := ":8080"
	log.Infof("starting cvib-research-meeting-calendar server on %s", addr)
	log.Fatal(http.ListenAndServe(addr, r))
}
