package main

import (
	"github.com/google/uuid"
	"time"
)

type session struct{
	username string
	sessionToken string
	authExpiresAt time.Time
	refreshToken string
	refreshExpiresAt time.Time
	next *session
}

var sessions = make(map[string]*session)
var refresh2session = make(map[string]string)

var authTokenExpiry = 5 * time.Minute
//var authTokenExpiry = 10 * time.Second
var refreshTokenExpiry = 8 * time.Hour

func NewSession(username string) *session {
	return &session{
		username: username,
		sessionToken: uuid.NewString(),
		authExpiresAt: time.Now().Add(authTokenExpiry),
		refreshToken: uuid.NewString(),
		refreshExpiresAt: time.Now().Add(refreshTokenExpiry),
		next: nil,
	}
}

func InvalidateChain(s *session) int {
	n := 0
	
	if s.next != nil {
		n = InvalidateChain(s.next)
	}

	delete(sessions, s.sessionToken)

	return n + 1
}