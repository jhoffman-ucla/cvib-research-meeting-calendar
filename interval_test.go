package main

import (
	"testing"
	"time"
)

func TestGetRemainer(t *testing.T) {
	// Main interval for comparison
	in1 := Interval{
		Start: time.Date(2000, 01, 02, 03, 0, 0, 0 , time.Local ),
		End:   time.Date(2000, 01, 02, 05, 0, 0, 0 , time.Local ),
	}

	// Same start time, end contained by in1
	in2 := Interval{
		Start: time.Date(2000, 01, 02, 03, 0, 0, 0 , time.Local ),
		End:   time.Date(2000, 01, 02, 04, 0, 0, 0 , time.Local ),
	}

	// Same start time, end contained by in1
	in7 := Interval{
		Start: time.Date(2000, 01, 02, 04, 0, 0, 0 , time.Local ),
		End:   time.Date(2000, 01, 02, 05, 0, 0, 0 , time.Local ),
	}
	
	// in3 == in1
	in3 := in1

	// Complete uncontained interval (no overlap)
	in4 := Interval{
		Start: time.Date(2000, 01, 02, 10, 0, 0, 0 , time.Local ),
		End:   time.Date(2000, 01, 02, 11, 0, 0, 0 , time.Local ),
	}

	//  in5 completely contains in1
	in5 := Interval{
		Start: time.Date(2000, 01, 02, 02, 0, 0, 0 , time.Local ),
		End:   time.Date(2000, 01, 02, 06, 0, 0, 0 , time.Local ),
	}

	// in6 completely contained by in1
	in6 := Interval{
		Start: time.Date(2000, 01, 02, 03, 30, 0, 0 , time.Local ),
		End:   time.Date(2000, 01, 02, 04, 30, 0, 0 , time.Local ),
	}

	// ==================================================
	// Sub interval 1
	res := in1.GetRemainder(in2)
	if len(res) != 1 {
		t.Log(res)
		t.Fatal("expected result to contain 1 interval")
	}
	
	if !res[0].Start.Equal(in2.End) {
		t.Error("expected result start to be 4pm")
	}

	if !res[0].End.Equal(in1.End) {
		t.Error("expected resultant end to be 5pm")
	}

	// ==================================================
	// Sub interval 2
	res = in1.GetRemainder(in7)
	t.Log(res)
	if len(res) != 1 {
		
		t.Fatal("expected result to contain 1 interval")
	}
	
	if !res[0].Start.Equal(in1.Start) {
		t.Error("expected result start to be 3pm")
	}

	if !res[0].End.Equal(in7.Start) {
		t.Error("expected resultant end to be 4pm")
	}


	// ==================================================
	// Intervals are equal
	res = in1.GetRemainder(in3)
	if res != nil {
		t.Error("expected nil but got ", res)
	}

	// ==================================================
	// Interval (in4) not contained at all by in1 (expect return to be in1)
	res = in1.GetRemainder(in4)
	if len(res) != 1 {
		t.Fatal("expected result to contain 1 interval")
	}

	if !res[0].Start.Equal(in1.Start) {
		t.Error("expected result start to be ", in1.Start)
	}

	if !res[0].End.Equal(in1.End) {
		t.Error("expected result end to be ", in1.End)
	}
	
	// ==================================================
	// Interval (in5) fully contains in1
	
	res = in1.GetRemainder(in5)
	if res != nil {
		t.Error("expected nil but got ", res)
	}

	// ==================================================
	// Interval (in6) fully contained by in1 (expect 2 resulting intervals subdividing in1)

	res = in1.GetRemainder(in6)
	if len(res) != 2 {
		t.Error("expected 2 intervals but got ", len(res))
	}

	tmp1 := res[0]
	tmp2 := res[1]

	if !tmp1.Start.Equal(in1.Start) {
		t.Error("tmp1 start != in1.Start")
	}

	if !tmp1.End.Equal(in6.Start) {
		t.Error("tmp1 start != in6.Start")
	}

	if !tmp2.Start.Equal(in6.End) {
		t.Log(tmp2)
		t.Log(in6)
		t.Error("tmp2 start != in6.End")
	}

	if !tmp2.End.Equal(in1.End) {
		t.Log(tmp2)
		t.Error("tmp2 start != in1.End")
	}
	
}

func TestInterval(t *testing.T) {

	start := time.Now()
	
	in := Interval{
		Start: start,
		End: start.Add(24 * time.Hour),
	}

	yesterday := start.AddDate(0,0,-1)

	if in.Contains(yesterday) {
		t.Error("interval should not contain yesterday")
	}

	sometime_soon := start.Add(10 * time.Hour)

	if !in.Contains(sometime_soon){
		t.Error("interval should contain sometime soon")
	}

	future := start.AddDate(0,0,10)
	if in.Contains(future){
		t.Error("interval should not contain future")
	}
	
} 