package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/renderer/html"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {

	authenticated := r.Context().Value("authenticated").(bool)

	tpl, err := template.New("index.tpl").Funcs(funcmap).ParseFiles("templates/index.tpl")
	if err != nil {
		http.Error(w, "failed to render index.tpl: "+err.Error(), http.StatusInternalServerError)
		return
	}

	data := struct {
		Upcoming      []Talk
		UpcomingFree  []Interval
		Past          []Talk
		Authenticated bool
	}{
		Upcoming:      GetUpcoming(),
		UpcomingFree:  GetUpcomingFree(),
		Past:          GetTalksBefore(time.Now()),
		Authenticated: authenticated,
	}

	err = tpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func PasscodeHandler(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)

	id, ok := params["id"]
	if !ok {
		http.Error(w, "ID not found", http.StatusBadRequest)
		return
	}

	talk := ds.Get(id)
	if talk == nil {
		http.Error(w, "Talk not found", http.StatusBadRequest)
		return
	}

	response := struct {
		Passcode string `json="passcode"`
	}{
		Passcode: PasswordString(talk.RecordingLink),
	}

	err := json.NewEncoder(w).Encode(response)
	if err != nil {
		log.Error(err)
	}

}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:

		// TODO: Insert templating here to pass the url with query
		// params to the form's action
		tpl, err := template.ParseFiles("templates/login.tpl")
		if err != nil {
			http.Error(w, "failed to parse login.tpl: "+err.Error(), http.StatusInternalServerError)
		}

		data := struct {
			URL string
		}{
			URL: r.URL.String(),
		}

		err = tpl.Execute(w, data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

	case http.MethodPost:

		fmt.Println(r.URL)

		r.ParseForm()

		expected_username := os.Getenv("CVIB_USER")
		expected_password := os.Getenv("CVIB_PASS")

		if r.FormValue("username") != expected_username || r.FormValue("password") != expected_password {
			log.Warn("Invalid login attempted")
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		log.Warn("login success")

		session := NewSession(r.FormValue("username"))

		http.SetCookie(w, &http.Cookie{
			Name:    "session_cookie",
			Value:   session.sessionToken,
			Expires: session.authExpiresAt,
			Path:    "/",
		})

		http.SetCookie(w, &http.Cookie{
			Name:    "refresh_cookie",
			Value:   session.refreshToken,
			Expires: session.refreshExpiresAt,
			Path:    "/",
		})

		refresh2session[session.refreshToken] = session.sessionToken
		sessions[session.sessionToken] = session

		redirect_url := r.FormValue("url")

		if redirect_url == "" {
			redirect_url = "/"
		}

		fmt.Println("redirect url: ", redirect_url)

		http.Redirect(w, r, redirect_url, http.StatusPermanentRedirect)

	default:
		http.Error(w, "method not allowed", http.StatusBadRequest)
	}
}

func RefreshHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	redirect_url := r.FormValue("next")

	// Comes to this handler already validated
	session_key := r.Context().Value("session_key").(string)
	//refresh_key := r.Context().Value("refresh_key").(string)

	// Retrieve the previous session
	prev_session, exists := sessions[session_key]
	if !exists {
		http.Error(w, "no valid session", http.StatusBadRequest)
		return
	}

	// Handle reuse
	already_used := (prev_session.next != nil)
	if already_used {
		n := InvalidateChain(prev_session)
		log.Warnf("Invalidated %d sessions because of attempted refresh token reuse (likely a bad actor)", n)
		http.Error(w, "refresh key reuse occurred.  check your computer security (this could be due to malicious behavior)", http.StatusForbidden)
		return
	}

	// Generate the new session and set cookies
	session := NewSession(prev_session.username)

	prev_session.next = session // update the previous session to show that we've used the refresh token

	// Issue new cookies
	http.SetCookie(w, &http.Cookie{
		Name:    "session_cookie",
		Value:   session.sessionToken,
		Expires: session.authExpiresAt,
		Path:    "/",
	})

	http.SetCookie(w, &http.Cookie{
		Name:    "refresh_cookie",
		Value:   session.refreshToken,
		Expires: session.refreshExpiresAt,
		Path:    "/",
	})

	// Add the new session to our tracker
	sessions[session.sessionToken] = session
	refresh2session[session.refreshToken] = session.sessionToken

	// Redirect to original resource
	http.Redirect(w, r, redirect_url, http.StatusFound)

	//w.WriteHeader(http.StatusOK)
}

func AddNewFormHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	date := r.FormValue("date")
	start := r.FormValue("start")
	end := r.FormValue("end")

	if start == "" {
		start = "15:00"
	}

	if end == "" {
		end = "17:00"
	}

	data := struct {
		Date  string
		Start string
		End   string
	}{
		Date:  date,
		Start: start[0:2] + ":" + start[2:4], // + ":" + start[4:6],
		End:   end[0:2] + ":" + end[2:4],     // + ":" + end[4:6],
	}

	tpl, err := template.New("addnew.tpl").Funcs(funcmap).ParseFiles("templates/addnew.tpl")
	if err != nil {
		http.Error(w, "failed to render addnew.html: "+err.Error(), http.StatusInternalServerError)
		return
	}

	err = tpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	//w.WriteHeader(http.StatusOK)
	//http.ServeFile(w, r, "addnew.html")
}

func EditHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	tpl, err := template.New("edit.tpl").Funcs(funcmap).ParseFiles("templates/edit.tpl")
	if err != nil {
		http.Error(w, "error loading edit.html: "+err.Error(), http.StatusInternalServerError)
		return
	}

	data := ds.Get(params["id"])

	err = tpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func DeleteHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	err := ds.Remove(params["id"])
	if err != nil {
		http.Error(w, "delete failed: "+err.Error(), http.StatusBadRequest)
	}
	ds.Save()
	//http.Redirect(w, r, "/", http.StatusPermanentRedirect)
}

func TalkHandler(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodGet:
		// Serve the resource.
	case http.MethodPost:

		err := r.ParseForm()
		if err != nil {
			http.Error(w, "Could not parse form data", http.StatusBadRequest)
		}

		fmt.Println(r.FormValue("date"))
		fmt.Println(r.FormValue("starttime"))
		fmt.Println(r.FormValue("endtime"))
		fmt.Println("private: ", r.FormValue("private"))

		//start, err := time.ParseInLocation("2006-01-02 03:04", r.FormValue("date")+" "+r.FormValue("starttime"), time.Local)
		start, err := time.ParseInLocation("2006-01-02 15:04", r.FormValue("date")+" "+r.FormValue("starttime"), time.Local)
		if err != nil {
			http.Error(w, "Could not parse start time, use exact formatting", http.StatusBadRequest)
			return
		}

		//end, err := time.ParseInLocation("2006-01-02 03:04", r.FormValue("date")+" "+r.FormValue("endtime"), time.Local)
		end, err := time.ParseInLocation("2006-01-02 15:04", r.FormValue("date")+" "+r.FormValue("endtime"), time.Local)
		if err != nil {
			http.Error(w, "Could not parse end time, use exact formatting", http.StatusBadRequest)
			return
		}

		id := r.FormValue("id")
		if id == "" {
			id = uuid.New().String()[0:8]
		}

		is_private := (r.FormValue("private") == "on")

		t := Talk{
			Id:            id,
			Interval:      Interval{Start: start, End: end},
			Speaker:       r.FormValue("speaker"),
			Affiliation:   r.FormValue("affiliation"),
			Title:         r.FormValue("title"),
			Description:   r.FormValue("description"),
			RecordingLink: r.FormValue("recordinglink"),
			Private:       is_private,
		}

		if ds.Get(id) != nil {
			ds.Update(t)
		} else {
			ds.Add(t)
		}

	case http.MethodDelete:

		// Remove the record.
		params := mux.Vars(r)
		err := ds.Remove(params["id"])
		if err != nil {
			http.Error(w, "delete failed: "+err.Error(), http.StatusBadRequest)
		}
		ds.Save()

		w.WriteHeader(http.StatusOK)
		return

	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}

	ds.Save()

	http.Redirect(w, r, "/", http.StatusPermanentRedirect)
}

func TalksHandler(w http.ResponseWriter, r *http.Request) {

	data, err := json.MarshalIndent(ds.GetAll(), "", "  ")
	if err != nil {
		http.Error(w, "failed to encode json: "+err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = w.Write(data)
	if err != nil {
		log.Error("write failed: " + err.Error())
		http.Error(w, "failed to encode json: "+err.Error(), http.StatusInternalServerError)
	}

	//json.NewEncoder(w).Encode(ds.GetAll())
}

func MarkdownHandler(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)

	page_name, ok := params["page"]
	if !ok {
		http.Error(w, "404: nope", http.StatusNotFound)
	}

	f, err := os.Open(filepath.Join("templates", page_name+".md"))
	if err != nil {
		http.Error(w, "404: nope", http.StatusNotFound)
		return
	}

	buff, err := io.ReadAll(f)
	if err != nil {
		http.Error(w, "this one's on me", http.StatusInternalServerError)
		return
	}

	md := goldmark.New(
		//goldmark.WithExtensions(extension.GFM),
		//goldmark.WithParserOptions(
		//	parser.WithAutoHeadingID(),
		//),
		goldmark.WithRendererOptions(
			html.WithHardWraps(),
			html.WithXHTML(),
			html.WithUnsafe(),
		),
	)

	err = md.Convert(buff, w)
	if err != nil {
		http.Error(w, "this one's on me", http.StatusInternalServerError)
		return
	}

}

func SchedulingHandler(w http.ResponseWriter, r *http.Request) {

	tpl, err := template.ParseFiles("templates/scheduling-email.tpl")
	if err != nil {
		http.Error(w, "could not find scheduling-email.tpl", http.StatusInternalServerError)
		return
	}

	slots := GetUpcomingFree()

	type SlotString struct {
		Date  string
		Start string
		End   string
	}

	slot_strings := make([]SlotString, len(slots))

	for i, s := range slots {
		slot_strings[i] = SlotString{
			Date:  s.Start.Format("January 2"),
			Start: s.Start.Format("3:04pm"),
			End:   s.End.Format("3:04pm"),
		}
	}

	err = tpl.Execute(w, slot_strings)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

func AnnouncementHandler(w http.ResponseWriter, r *http.Request) {
	
	type TalkString struct {
		Date          string
		Start         string
		End           string
		Speaker       string
		Affiliation   string
		Title         string
		Description   string
		RecordingLink string
		Private       bool
	}

	type Announcements struct {
		NumTalks    int
		DateAndTime string
		NumTalksStr string
		Talks       []TalkString
		Upcoming    []TalkString
		Recordings  []TalkString
	}

	stringifyTalk := func(t_in Talk) TalkString {
		return TalkString{
			Date:          t_in.Interval.Start.Format("01/02/2006"),
			Start:         t_in.Interval.Start.Format("3:04"),
			End:           t_in.Interval.End.Format("3:04"),
			Speaker:       t_in.Speaker,
			Affiliation:   t_in.Affiliation,
			Title:         t_in.Title,
			Description:   t_in.Description,
			RecordingLink: t_in.RecordingLink,
			Private:       t_in.Private,
		}
	}

	// Fetch the info about the upcoming week
	//ref_time := time.Now().Add( -time.Duration(12 * time.Hour))
	ref_time := time.Now()

	talks := GetNextNWeeksTalks(ref_time, 1)
	upcoming := GetNextNWeeksTalks(ref_time.AddDate(0, 0, 3), 1)
	recordings := GetNextNWeeksTalks(ref_time.AddDate(0, 0, -7), 1)

	talks_str := make([]TalkString, 0)
	upcoming_str := make([]TalkString, 0)
	recordings_str := make([]TalkString, 0)

	num_words := []string{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"}

	for _, t := range talks {
		talks_str = append(talks_str, stringifyTalk(t))
	}

	for _, t := range upcoming {
		upcoming_str = append(upcoming_str, stringifyTalk(t))
	}

	for _, t := range recordings {
		recordings_str = append(recordings_str, stringifyTalk(t))
	}

	datetime_str := ""
	if len(talks) > 0 {
		datetime_str = talks[0].Interval.Start.Format("January 2") + " " + talks[0].Interval.Start.Format("3:04") + "-" + talks[len(talks)-1].Interval.End.Format("3:04")
	}

	announcements := Announcements{
		NumTalks:    len(talks),
		DateAndTime: datetime_str,
		NumTalksStr: num_words[len(talks)],
		Talks:       talks_str,
		Upcoming:    upcoming_str,
		Recordings:  recordings_str,
	}

	// Render the template
	tpl, err := template.ParseFiles("templates/announcement-email.tpl")
	if err != nil {
		http.Error(w, "error loading announcement-email.tpl: "+err.Error(), http.StatusInternalServerError)
		return
	}

	err = tpl.Execute(w, announcements)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

func NoMeetingHandler(w http.ResponseWriter, r *http.Request) {

	meeting_day := GetNextThursday(time.Now())
	
	data := struct {
		DateAndTime string
	}{
		DateAndTime: meeting_day.Format("Jan 2, 2006"),
	}

	tpl, err := template.New("no-meeting.tpl").Funcs(funcmap).ParseFiles("templates/no-meeting.tpl")
	if err != nil {
		http.Error(w, "failed to render no-meeting.html: "+err.Error(), http.StatusInternalServerError)
		return
	}

	tpl.Execute(w, data)

}