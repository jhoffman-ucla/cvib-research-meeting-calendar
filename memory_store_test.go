package main

import (
	"testing"
	"time"
)

func runDataStoreTest(ds DataStore, t *testing.T) {
	
	start := time.Date(2022, 12, 1, 15, 0, 0, 0, time.Local)
	end := time.Date(2022, 12, 1, 16, 0, 0, 0, time.Local)

	talk := Talk{
		Interval: Interval{
			Start: start,
			End: end,
		},
		Speaker: "John Hoffman",
		Affiliation: "CVIB/UCLA",
		Title: "How to make managing the research meeting a little bit easier",
		Description: "In this talk we'll discuss this here application",
	}

	// ====================
	
	err := ds.Add(talk) // expect success
	if err != nil {
		t.Error(err)
	}

	err = ds.Add(talk) // expect error
	if err == nil {
		t.Errorf("expected add to fail, but succeeded")
	}

	// ====================

	talk2 := talk
	talk2.Speaker = "Cookie Monster"
	talk2.Affiliation = "Sesame Street"
	
	err = ds.Update(talk2) // expect success
	if err != nil {
		t.Error(err)
	}

	talk2.Interval.Start = time.Now()
	err = ds.Update(talk2) // expecte failure
	if err==nil {
		t.Error("expected update to fail, but succeeded")
	}
	
	// ====================

	v := ds.Get(start)
	if v==nil {
		t.Error("expected retrieval to succeed, but returned nil")
	}

	t.Log(v)

	v = ds.Get(time.Now())
	if v != nil {
		t.Error("expected nil, but got ", v)
	}

	t.Log(v)

	// ====================

	err = ds.Remove(talk2.Interval.Start) // expect failure
	if err == nil {
		t.Error("expected error, but got nil")
	}

	err = ds.Remove(talk.Interval.Start) // expect success
	if err !=nil {
		t.Error(err)
	}

	// ==================== 

	v = ds.Get(start)

	if v != nil {
		t.Error("expected talk to have been removed from data store but was not")
	}
}

func TestMemoryStore(t *testing.T) {
	m := NewMemoryStore()
	runDataStoreTest(m, t)
}