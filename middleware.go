package main

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		start := time.Now()

		next.ServeHTTP(w, r)

		elapsed := time.Since(start)
		log.WithFields(log.Fields{
			"url":      r.URL,
			"method":   r.Method,
			"protocol": r.Proto,
			"remote":   r.RemoteAddr,
		}).Infof("Elapsed: %v", elapsed)

	})
}

// VerifyCookiesMiddleware reads the session and refresh cookies and
// sets context values reflecting their validity. No actual action is taken.
func VerifyCookiesMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		log.Info("VerifyCookiesMiddleware invoked")

		session_valid := false
		refresh_valid := false

		session_cookie, err := r.Cookie("session_cookie")
		if err != nil {
			session_valid = false
		}

		refresh_cookie, err := r.Cookie("refresh_cookie")
		if err != nil {
			refresh_valid = false
		}

		if session_cookie != nil {
			sess, exists := sessions[session_cookie.Value]

			if !exists {
				session_valid = false
			} else {
				if time.Now().Before(sess.authExpiresAt) {
					session_valid = true
				}
			}
		}

		session_key := ""
		exists := false
		if refresh_cookie != nil {
			session_key, exists = refresh2session[refresh_cookie.Value]
			if !exists {
				refresh_valid = false
			} else {
				sess, exists := sessions[session_key]
				if !exists {
					refresh_valid = false
				} else {
					if time.Now().Before(sess.refreshExpiresAt) {
						refresh_valid = true
					}
				}
			}
		}

		// Log any weird states we detect
		if session_valid && !refresh_valid {
			log.Warn("session valid, but refresh invalid; this should not occur.")
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, "session_valid", session_valid)
		ctx = context.WithValue(ctx, "refresh_valid", refresh_valid)

		if session_valid {
			ctx = context.WithValue(ctx, "session_key", session_cookie.Value)
		}

		if refresh_valid {
			ctx = context.WithValue(ctx, "refresh_key", refresh_cookie.Value)
			ctx = context.WithValue(ctx, "session_key", session_key)
		}

		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		log.Info("AuthMiddleware invoked")

		authenticated := false

		defer func() {
			ctx := context.WithValue(r.Context(), "authenticated", authenticated)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		}()

		session_valid := r.Context().Value("session_valid").(bool)
		refresh_valid := r.Context().Value("refresh_valid").(bool)

		if session_valid && refresh_valid {
			authenticated = true
			return
		}

		if !session_valid && refresh_valid {
			redirect_url := fmt.Sprintf("/refresh?next=%s", r.URL.String())
			log.Infof("redirecting to: %s", redirect_url)
			http.Redirect(w, r, redirect_url, http.StatusFound)
		}

	})
}

func ProtectedMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		log.Info("ProtectedMiddleware invoked")

		if !r.Context().Value("authenticated").(bool) {
			log.Info("must log in first")
			fmt.Println(r.URL)
			http.Redirect(w, r, fmt.Sprintf("/login?url=%s", r.URL), http.StatusFound)
		}

		next.ServeHTTP(w, r)
	})
}
