module gitlab.com/jhoffman-ucla/cvib-research-meeting-calendar

go 1.18

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/joncalhoun/form v1.0.1 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/yuin/goldmark v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
