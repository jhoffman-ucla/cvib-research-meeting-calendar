
function confirmdelete(id) {
    if (confirm("Are you sure you want to delete the talk (" + id + ")?") == true) {
        let url = '/talk/' + id;
        console.log(url);
        fetch(url, {method:"delete"}).then(response => window.location.replace("/"));
    }
    else {}
    //window.location.replace("/");
}


function retrievepasscode(element, id) {
	let url = '/pass/' + id;
	fetch(url, {method:"get"})
		.then((response) => response.json())
		.then((data) => {
			console.log(data);
			element.innerHTML = data["Passcode"];
			element.style.color = "#555";
			element.style.textDecoration = "none";
		});
}