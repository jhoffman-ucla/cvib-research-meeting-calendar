package main

import (
	//"fmt"
	//"html/template"
	"sort"
	"time"
)

func GetNextThursday(t time.Time) time.Time {
	if t.Weekday() == time.Thursday {
		return t.AddDate(0, 0, 7)
	}

	for {
		if t.Weekday() == time.Thursday {
			return t
		}
		t = t.AddDate(0, 0, 1)
	}

	return t
}

func GetNextNWeeksTalks(t time.Time, n int) []Talk {
	g := ds.GetAll()

	// Select the talks that fall within the next five weeks
	to_show := make([]Talk, 0, 20)
	hrs_five_wks := float64(n * 7 * 24)

	for _, v := range g {
		time_until := v.Interval.Start.Sub(t)
		if time_until.Hours() > hrs_five_wks || time_until.Hours() < 0 {
			continue
		}
		to_show = append(to_show, v)
	}

	sort.Slice(to_show, func(i, j int) bool {
		return to_show[i].Interval.Start.Before(to_show[j].Interval.Start)
	})

	return to_show
}

func GetNextNWeeksSlots(t time.Time, n int) []Interval {
	slots := make([]Interval, n)

	th := t

	for i := 0; i < n; i++ {
		th = GetNextThursday(th)

		inter := Interval{
			Start: time.Date(th.Year(), th.Month(), th.Day(), 15, 0, 0, 0, time.Local),
			End:   time.Date(th.Year(), th.Month(), th.Day(), 17, 0, 0, 0, time.Local),
		}

		slots[i] = inter
	}

	return slots
}

func GetTalksBefore(t time.Time) []Talk {

	talks := make([]Talk, 0)
	for _, talk := range ds.GetAll() {
		if talk.Interval.Start.Before(t) {
			talks = append(talks, talk)
		}
	}

	sort.Slice(talks, func(i, j int) bool { return talks[i].Interval.Start.After(talks[j].Interval.Start) })

	return talks
}

func GetUpcoming() []Talk {
	to_show := GetNextNWeeksTalks(time.Now(), 20)
	return to_show
}

func GetUpcomingFree() []Interval {

	// Generate our slice of slots
	n_weeks := 20
	talks := GetNextNWeeksTalks(time.Now(), n_weeks)
	slots := GetNextNWeeksSlots(time.Now(), n_weeks)

	for _, tl := range talks {
		slots = removeInterval(tl.Interval, slots)
	}

	sort.Slice(slots, func(i, j int) bool { return slots[i].Start.Before(slots[j].Start) })

	return slots[0:5]
}
