package main

import (
	"strings"
	"time"
)

type Talk struct {
	Id            string   `json:"id"`
	Interval      Interval `json:"interval"`
	Speaker       string   `json:"speaker"`
	Affiliation   string   `json:"affiliation"`
	Title         string   `json:"title"`
	Description   string   `json:"description"`
	RecordingLink string   `json:"RecordingLink"`
	Private       bool     `json:"private"`
}

var funcmap = map[string]any{
	"shortdate":      ShortDate,
	"fulldate":       FullDate,
	"shorttime":      ShortTime,
	"formdate":       FormDate,
	"formtime":       FormTime,
	"formtimecolons": FormTimeColons,
	"edittime":       EditTime,
	"passwd":         PasswordString,
}

func ShortDate(t time.Time) string {
	return t.Format("Jan 2, 2006")
}

func FullDate(t time.Time) string {
	return t.Format("January 2, 2006")
}

func ShortTime(t time.Time) string {
	//return t.Format("15:04")
	return t.Format("3:04")
}

func FormDate(t time.Time) string {
	return t.Format("2006-01-02")
}

func FormTime(t time.Time) string {
	return t.Format("1504")
}

func FormTimeColons(t time.Time) string {
	return t.Format("15:04")
}

func EditTime(t time.Time) string {
	return t.Format("2006/01/02 15:04")
}

func PasswordString(link string) string {

	sub_str := strings.Split(link, " ")

	if len(sub_str) < 3 {
		return "n/a"
	}

	return sub_str[2]
}

func (t Talk) GetShortDateString() string {
	return t.Interval.Start.Format("Jan 2, 2006")
}

func (t Talk) GetFullDateString() string {
	return t.Interval.Start.Format("January 2, 2006")
}

func (t Talk) GetStartTimeString() string {
	return t.Interval.Start.Format("15:04")
}

func (t Talk) GetEndTimeString() string {
	return t.Interval.Start.Format("15:04")
}

//func (t Talk) GetYYYYMMDDDateString() string {
//	return t.Interval.Start.Format("2006/")
//}
