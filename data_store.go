package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
)

type DataStore interface {
	Add(Talk) error
	Update(Talk) error
	Remove(string) error // Start time is the UID for all entries
	Get(string) *Talk
	GetAll() map[string]Talk

	Save() error
	Load() error
}

type memoryStore struct {
	Data map[string]Talk
}

func NewMemoryStore() *memoryStore {
	return &memoryStore{
		Data: make(map[string]Talk),
	}
}

func (m *memoryStore) Add(t Talk) error {

	_, exists := m.Data[t.Id]
	if exists {
		return errors.New("talk already exists")
	}

	m.Data[t.Id] = t

	return nil
}

func (m *memoryStore) Update(t Talk) error {

	_, exists := m.Data[t.Id]
	if !exists {
		return errors.New("talk to be updated does not exist")
	}

	m.Data[t.Id] = t

	return nil
}

func (m *memoryStore) Remove(id string) error {

	fmt.Println("remove:", id)

	_, exists := m.Data[id]
	if !exists {
		return errors.New("talk does not exist")
	}

	delete(m.Data, id)

	return nil
}

func (m *memoryStore) Get(id string) *Talk {

	talk, exists := m.Data[id]
	if !exists {
		return nil
	}

	return &talk
}

func (m *memoryStore) GetAll() map[string]Talk { // risky as is, b/c we can modify the underlying map

	data := make(map[string]Talk)

	for k, v := range m.Data {
		data[k] = v
	}

	return data
}

func (m *memoryStore) Save() error {
	f, err := os.Create(db_save_path)
	if err != nil {
		return err
	}

	data, err := json.MarshalIndent(m.Data, "", " ")
	if err != nil {
		return err
	}

	_, err = f.Write(data)
	if err != nil {
		return err
	}

	return nil
}

func (m *memoryStore) Load() error {

	log.Infof("loading db from %s", db_save_path)

	f, err := os.Open(db_save_path)
	if errors.Is(err, fs.ErrNotExist) {
		return nil
	}

	if err != nil {
		return err
	}

	buff := make([]byte, 10*1000000) // 10 MB should be plenty for a while...

	n_bytes, err := f.Read(buff)

	buff = buff[0:n_bytes]

	log.Infof("loaded %d bytes from disk\n", n_bytes)
	//fmt.Printf("%s\n", string(buff))

	err = json.Unmarshal(buff, &m.Data)

	// Hacky fix to make sure we're not actually using AM times
	for k, v := range m.Data {

		t_start := v.Interval.Start
		t_end := v.Interval.End

		if t_start.Hour() < 12 {
			log.Info("Shifting time for talk ", k)
			v.Interval.Start = t_start.Add(12 * time.Hour)
			v.Interval.End = t_end.Add(12 * time.Hour)
		}

		m.Data[k] = v
	}

	return err
}
