package main

import (
	"testing"
	"time"
)

func TestGetNextThursday(t *testing.T) {

	tm, err := time.Parse("2006/01/02", "2022/07/18") // a monday
	if err != nil {
		t.Error(err)
	}

	th := GetNextThursday(tm)

	if th.Day() != 21 {
		t.Error("Expected the 21st but got ", th.Day())
	}

	th = GetNextThursday(th)
	if th.Day() != 28 {
		t.Error("Expected the 28th but got ", th.Day())
	}

}
